﻿using System;
using System.Windows;
using System.Diagnostics;

namespace CometinPermissionGranter
{
    public partial class ADBGuide
    {
        public ADBGuide()
        {
            InitializeComponent();
        }

        private void Window_SourceInitialized(object sender, EventArgs e)
        {
        }

        private void PixelButton_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://telegra.ph/Enable-developer-options-and-debugging-11-02");
        }

        private void SamsungButton_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://telegra.ph/Enable-developer-options-and-debugging-11-02-2");
        }

        private void HuaweiButton_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://telegra.ph/Enable-developer-options-and-debugging-11-02-3");
        }

        private void XiaomiButton_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://telegra.ph/Enable-developer-options-and-debugging-11-02-4");
        }

        private void OtherNewButton_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://telegra.ph/Enable-developer-options-and-debugging-11-02-5");
        }

        private void OtherOldButton_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://telegra.ph/Enable-developer-options-and-debugging-11-02"); 
        }
    }
}