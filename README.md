# CometinPermissionGranter

**Download**
[All releases](https://gitlab.com/Stjin/cometinpermissiongranter/-/releases) 

**Forked**
This project is a forked project from [Dubzer's FluidNGPermissionGranter](https://github.com/Dubzer/FluidNGPermissionGranter)


**Building**
 - Clone the repository
 - Open **CometinPermissionGranter.sln** in Visual Studio 2017+
 - Click **Run** on the top of VS window
 - Open folder where build stored and unpack the adb archive in it. You will get this hierarchy:
 
 - After that you can safely remove **.config** and **.pdb** files 

